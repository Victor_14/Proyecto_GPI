<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormulariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formularios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function getmyArea($habilidades, $competencias){
       $rank4Area = array();
       $mySkills = 0;
       $myStrengths = 0;    
       for ($i=0; $i <6 ; $i++) { 
         if($i==0){
            $mySkills= $habilidades['disciplinado'] + $habilidades['organizador']
            + $habilidades['estrategico'] + $habilidades['restaurador'];
            $myStrengths =  $competencias['unity'] + $competencias['lenguajes_programacion'] + $competencias['matematicas'];
            $rank4Area['IT'] = round(($mySkills + $myStrengths)/700,4);
         }
         if ($i==1) {
           $mySkills = $habilidades['imaginativa'] + $habilidades['futurista']
            + $habilidades['organizador'] + $habilidades['enfoque'];
           $myStrengths = $competencias['matematicas'] + $competencias['ofimatica'] +
          $competencias['photoshop'];
           $rank4Area['Marketing'] = round(($mySkills + $myStrengths)/700,4);
         }
         if ($i==2) {
           $mySkills = $habilidades['imaginativa'] + $habilidades['futurista']
            + $habilidades['excelencia'];
           $myStrengths = $competencias['blender'] + $competencias['unity'];
           $rank4Area['Arte'] = round(($mySkills + $myStrengths)/500, 4);
         }
         if ($i==3) {
           $mySkills = $habilidades['excelencia'] + $habilidades['disciplinado']
            + $habilidades['enfoque'];
           $myStrengths = $competencias['unity'] + $competencias['lenguajes_programacion'];
           $rank4Area['Calidad'] = round( ($mySkills + $myStrengths)/500, 4);
         }
         if ($i==4) {
           $mySkills = $habilidades['imaginativa'] + $habilidades['armonico']
            + $habilidades['iniciador'];
           $myStrengths = $competencias['ofimatica'];
           $rank4Area['Creativa'] = round(($mySkills + $myStrengths)/400, 4);
         }
         if ($i==5) {
           $mySkills = $habilidades['imaginativa'] + $habilidades['responsabilidad']
            + $habilidades['excelencia'] + $habilidades['armonico'];
           $myStrengths = $competencias['blender'] + $competencias['unity'] +
          $competencias['photoshop'];
           $rank4Area['Diseño_de_Niveles'] = round(($mySkills + $myStrengths)/700, 4);
         }
         $mySkills = 0;
         $myStrengths = 0;  
       }
      return $rank4Area;
     }


     public function selBestArea($rank4Area){
        arsort($rank4Area);
        $bestArea = array();
        $max=0;
        $maxKey = '';
        foreach ($rank4Area as $key => $value) {
          if($value > $max){
            $max = $value;
            $maxKey= $key;
          }
          else{
            if($value == $max)
              array_push($bestArea, $key);
          }
        }

        array_push($bestArea, $maxKey);
        return $bestArea;
      }

      public function seleccionarRol($bestArea, $mejorJerarquia){
          $rol = array();
          //array_push($rol,) '';
            foreach ($bestArea as $key => $value) {
                if($value=='IT'){
                  if($mejorJerarquia=='operativo'){
                    array_push($rol,'Desarrollador') ;
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol, 'Jefe de Desarrolladores');
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,'Director de IT');
                  } 
                }
                if ($value=='Marketing') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol, 'Publicista');
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,'Jefe de publicidad');
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,'Director de Finanzas');
                  } 
                }
                if ($value=='Arte') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol, 'Diseñador en 3d');
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol, 'Jefe de Diseño en 3d');
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol, 'Director de Arte');
                  } 
                }
                if ($value=='Calidad') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol, 'Game Tester');
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,'Jefe de Producto');
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol, 'Director de Calidad');
                  } 
                }
                if ($value=='Creativa') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol, 'Guionista');
                  }elseif ($mejorJerarquia=='tactico'){
                    array_push($rol, 'Guionista Jefe');
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol, 'Director Creativo');
                  } 
                }
                if ($value=='Diseño_de_Niveles') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol, 'Diseñador de Niveles');
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol, 'Jefe de diseño de niveles');
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol, 'Director de Diseño');
                  } 
                }
              }
              return $rol;
          
      }

      /*
       $rol = array();
          array_push($rol,) '';
            foreach ($bestArea as $key => $value) {
                if($key=='IT'){
                  if($mejorJerarquia=='operativo'){
                    array_push($rol,) 'Desarrollador';
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,) 'Jefe de Desarrolladores';
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,) 'Director de IT';
                  } 
                }
                if ($key=='Finanzas') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol,) 'Publicista';
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,) 'Jefe de publicidad';
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,) 'Director de Finanzas';
                  } 
                }
                if ($key=='Arte') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol,) 'Diseñador en 3d';
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,) 'Jefe de Diseño en 3d';
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,) 'Director de Arte';
                  } 
                }
                if ($key=='Calidad') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol,) 'Game Tester';
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,) 'Jefe de Producto';
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,) 'Director de Calidad';
                  } 
                }
                if ($key=='Creativa') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol,) 'Guionista';
                  }elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,) 'Guionista Jefe';
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,) 'Director Creativo';
                  } 
                }
                if ($key=='Diseño_de_Niveles') {
                  if($mejorJerarquia=='operativo'){
                    array_push($rol,) 'Diseñador de Nivelesr';
                  } elseif ($mejorJerarquia=='tactico'){
                    array_push($rol,) 'Jefe de diseño de niveles';
                  }elseif ($mejorJerarquia=='gerencial'){
                    array_push($rol,) 'Director de Diseño';
                  } 
                }
              }
              return $rolUnique;
      
      */


    public function selHierarchy($mejorJerarquia){
        $maxValue = max($mejorJerarquia);
        $bestHierarchy = array_search($maxValue, $mejorJerarquia);
        return $bestHierarchy;
    }


    public function getHierarchy($necesidades){
      $mejorJerarquia = array('tactico'=>'', 'operativo'=>'',
      'gerencial'=>'' );
      $sumTactico = 0;
      $sumEstrategico = 0;
      $sumOperativo = 0;
      foreach ($necesidades as $key => $value) {
        if($key=='Poder'){
            if($value=='Influir'){
              $sumEstrategico++; 
            }elseif($value=='Obligar'){
              $sumTactico++;
            }
            elseif($value=='Destacar'){
              $sumOperativo++;
            }
        }
        if($key=='Autorrealizacion' && $value==1){
            $sumEstrategico++;
        }
        if($key=='Seguridad' && $value==1){
            $sumOperativo++;
        }
        if($key=='X' && $value==1){
            $sumOperativo++;
        }
        if($key=='Y' && $value==1){
            $sumTactico++;
            $sumEstrategico++;
        }
        if($key=='FE-Sueldo' && $value==1){
          $sumOperativo++;
        }
        if($key=='FE-Progreso' && $value==1){
          $sumTactico++;
        }
        if($key=='FE-Supervision' && $value==1){
          $sumTactico++;
        }
        if($key=='FE-Administracion' && $value==1){
           $sumEstrategico++;    
        }    
      }

        $mejorJerarquia['gerencial'] = $sumEstrategico;
        $mejorJerarquia['tactico']= $sumTactico;
        $mejorJerarquia['operativo'] =$sumOperativo;

        return $mejorJerarquia;
    }



    public function store(Request $request){

        $input=$request->all();
        $habilidades=array();
        $ct=1;
        $habilidades['armonico']=0;
        $habilidades['disciplinado']=0;
        $habilidades['enfoque']=0;
        $habilidades['estrategico']=0;
        $habilidades['excelencia']=0;
        $habilidades['futurista']=0;
        $habilidades['imaginativa']=0;
        $habilidades['iniciador']=0;
        $habilidades['organizador']=0;
        $habilidades['responsabilidad']=0;
        $habilidades['restaurador']=0;

        $necesidades['Poder']=0;
        $necesidades['Maslow']=0;
        $necesidades['Autorrealizacion']=0;
        $necesidades['Seguridad']=0;

        $necesidades['Competitividad']=0;
        $necesidades['Question27']=0;
        $necesidades['Question28']=0;
        $necesidades['Question29']=0;
        $necesidades['X']=0;
        $necesidades['Y']=0;
        $necesidades['FE-Sueldo']=0;
        $necesidades['FE-Progreso']=0;
        $necesidades['FE-Supervision']=0;
        $necesidades['FE-Administracion']=0;

        $competencias['photoshop'] = 0;
        $competencias['blender'] = 0;
        $competencias['matematicas'] = 0;
        $competencias['ofimatica'] = 0;
        $competencias['unity'] = 0;
        $competencias['lenguajes_programacion'] = 0;  


        //dd($input);
        foreach ($input as $key => $re) {
          if($re!='si' && $re!='no'){
            if($ct==5){
              if($re=='a')
                $habilidades['enfoque']=$habilidades['enfoque']+((1/2)*100);

              if($re=='b')
                $habilidades['enfoque']=$habilidades['enfoque']+((0.375)*100);

              if($re=='c')
                $habilidades['enfoque']=$habilidades['enfoque']+((1/4)*100);

              if($re=='d')
                $habilidades['enfoque']=$habilidades['enfoque']+((1/8)*100);

              if($re=='e')
                $habilidades['enfoque']=$habilidades['enfoque']+0;
            }

            if($ct==24){
              if($re=='a')
                $necesidades['Poder']='Influir';
              if($re=='b')
                $necesidades['Poder']='Obligar'; //Hacer que otros hagan lo que no harían
              if($re=='c')
                $necesidades['Poder']='Destacarse';
            }

          }else{
            if($ct<=2 &&  $re=='si'){
              $habilidades['armonico']=$habilidades['armonico']+((1/2)*100);
            }
            if($ct>2 && $ct<=4 &&  $re=='si'){
              $habilidades['disciplinado']=$habilidades['disciplinado']+((1/2)*100);
            }
            if($ct==6 &&  $re=='si'){
              $habilidades['enfoque']=$habilidades['enfoque']+((1/2)*100);
            }
            if($ct>6 && $ct<=8 &&  $re=='si'){
              $habilidades['estrategico']=$habilidades['estrategico']+((1/2)*100);
            }
            if($ct>8 && $ct<=10 &&  $re=='si'){
              $habilidades['excelencia']=$habilidades['excelencia']+((1/2)*100);
            }
            if($ct>10 && $ct<=12 &&  $re=='si'){
              $habilidades['imaginativa']=$habilidades['imaginativa']+((1/2)*100);
            }
            if($ct>12 && $ct<=14 &&  $re=='si'){
              $habilidades['futurista']=$habilidades['futurista']+((1/2)*100);
            }
            if($ct>14 && $ct<=16 &&  $re=='si'){
              $habilidades['iniciador']=$habilidades['iniciador']+((1/2)*100);
            }
            if($ct>16 && $ct<=18 &&  $re=='si'){
              $habilidades['organizador']=$habilidades['organizador']+((1/2)*100);
            }
            if($ct>18 && $ct<=20 &&  $re=='si'){
              $habilidades['responsabilidad']=$habilidades['responsabilidad']+((1/2)*100);
            }
            if($ct>20 && $ct<=22 &&  $re=='si'){
              $habilidades['restaurador']=$habilidades['restaurador']+((1/2)*100);
            }

            if($ct==23 &&  $re=='si'){
              $necesidades['Competitividad']=100;
            }

            if($ct==25 &&  $re=='si'){
              $necesidades['Maslow']=$necesidades['Maslow']+((1/2)*100);
            }

            if($ct==26 &&  $re=='si'){
              $necesidades['Maslow']=$necesidades['Maslow']+((1/2)*100);
            }

            if($ct==27 &&  $re=='si'){
              $necesidades['Question27']=1;
            }

            if($ct==28 &&  $re=='si'){
              $necesidades['Question28']=1;
            }

            if($ct==29 &&  $re=='si'){
              $necesidades['Question29']=1;
            }


            if($ct==30){
              if($re=='si'){
                $necesidades['FE-Sueldo']=1;
              }
              else{
                $necesidades['FE-Progreso']=1;
              }
            }

            if($ct>30 && $ct<=32 &&  $re=='si'){
              $necesidades['FE-Supervision']=1;
            }
            if($ct==33 &&  $re=='si'){
              $necesidades['FE-Administracion']=1;
            }

            if($ct==34 && $re=='si'){
              $competencias['photoshop']=$competencias['photoshop']+(1*100);
            }
            if($ct==35 && $re=='si'){
              $competencias['blender']=$competencias['blender']+(1*100);
            }
            if($ct==36 && $re=='si'){
              $competencias['matematicas']=100;
            }
            if($ct==37 && $re=='si'){
              $competencias['ofimatica']=100;
            }
            if($ct==38 && $re=='si'){
              $competencias['unity']=100;
            }
            if($ct==39 && $re=='si'){
              $competencias['lenguajes_programacion']=1;
            }
          }
          $ct++;
        }

        if($necesidades['Maslow']==100){
              $necesidades['Autorrealizacion']=1;
        }elseif ($necesidades['Maslow']<100){
              $necesidades['Seguridad']=1;
        }

        if($necesidades['Question27']==1 && $necesidades['Question28']==1 && $necesidades['Question29']==1 ||
        $necesidades['Question27']==0 && $necesidades['Question28']==1 || $necesidades['Question27']==1 && $necesidades['Question28']==1 
        && $necesidades['Question29']==1){
            $necesidades['X']=1;
        }else{
          $necesidades['Y']=1;
        }
        //dd($necesidades,$competencias);
        $myArea = $this->getmyArea($habilidades, $competencias);
        $bestArea = $this->selBestArea($myArea);
        $bestHierarchy = $this->getHierarchy($necesidades);
        $mejorJerarquia = $this->selHierarchy($bestHierarchy);
        $miRol= $this->seleccionarRol($bestArea, $mejorJerarquia);
        //dd($necesidades, $competencias,$habilidades,$myArea, $bestArea, $bestHierarchy, $fitHierarchy, $miRol);

        return view('formularios.resultados')->with('necesidades',$necesidades)->with('habilidades',$habilidades)->with('competencias',$competencias)->
        with('myArea',$myArea)->with('bestArea',$bestArea)->with('jerarquias',$bestHierarchy)->with('mejorJerarquia',$mejorJerarquia)->with('miRol',$miRol);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function procesarDatos(){

    }
}
