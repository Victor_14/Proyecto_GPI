<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Proyecto GPI @yield('title') </title>
        <link href="{{asset('plugins/bootstrap-3.3.7/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>

        <!-- BOOTSTRAP -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"> </script>
        @stack('css-head')

    </head>
    <body>
    	<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" >GPI</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li id="home-li"><a href="{{ asset('/') }}"><i class="fa fa-home" aria-hidden="true"></i> Home <!--<span class="sr-only">(current)</span>--></a></li>
		        <li id="formularios-li"><a href="{{ asset('/formularios') }}"><i class="fa fa-file-text-o" aria-hidden="true"></i> Perfil segun cuestionario</a></li>
		      </ul>

		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>

		<div class="panel panel-default">
		  <div class="panel-body" style="padding: 36px !important">
		    @yield('content')
		  </div>
		</div>

    </body>
    <script type="text/javascript" src="{{ asset('plugins/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-3.3.7/js/bootstrap.min.js') }}"></script>

    @stack('script-footer')
</html>
