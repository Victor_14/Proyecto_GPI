@extends('master')

@section('title','Home')

@section('content')
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title"><i class="fa fa-home" aria-hidden="true"></i> Home</h3>
	  </div>
	  <div class="panel-body">
	  	<center>
	  		<h4>Bienvenido, elija la opción que necesita</h4>
	  		<a href="{{ asset('/formularios') }}" class="btn btn-primary btn-lg" role="button"><i class="fa fa-file-text-o" aria-hidden="true"></i> Perfil segun cuestionario</a>
	  	</center>
	    
	    
	  </div>
	</div>


@endsection

@push('script-footer')


<script type="text/javascript">
	$(document).ready(function(){

		$("#home-li").addClass( "active" );
		
	});
</script>
@endpush