@extends('master')

@section('title','Resultados')

@section('content')
	<div class="panel panel-default">
    <div class="panel panel-body">

      <div class="col-md-3">
      <h4>Mejor Jerarquía</h4>
      <p>{{$mejorJerarquia}}</p>
      </div>

      @if(count($bestArea)>=2)
			<div class="col-md-3">
				<table class="table table-condensed">
					<thead>
						<th>Tus Áreas óptimas de trabajo son:</th>
					</thead>
						@foreach($bestArea as $key => $value)
						<tbody>
							@if ($value=='Diseño_de_Niveles')
									<td>Diseño de Niveles</td>
							@else
									<td>{{$value}}</td>
							@endif
							</tbody>
						@endforeach
				</table>
			</div>
			<div class="col-md-3">

				<table class="table table-condensed">
					<thead>
						<th>Tus posibles Roles en la Empresa serían:</th>
					</thead>
						@foreach($miRol as $key => $value)
						<tbody>
							<td>{{$value}}</td>
							</tbody>
						@endforeach
				</table>
			</div>

      @else

			<div class="col-md-3">
				<table class="table table-condensed">
					<thead>
						<th>Tu Área ideal de trabajo es:</th>
					</thead>
						@foreach($bestArea as $key => $value)
						<tbody>
							@if ($value=='Diseño_de_Niveles')
									<td>Diseño de Niveles</td>
							@else
									<td>{{$value}}</td>
							@endif
							</tbody>
						@endforeach
				</table>
			</div>

			<div class="col-md-3">
				<table class="table table-condensed">
					<thead>
						<th>Tu rol en la Empresa es:</th>
					</thead>
						@foreach($miRol as $key => $value)
						<tbody>
							<td>{{$value}}</td>
							</tbody>
						@endforeach
				</table>
			</div>
      @endif

    </div>

		<button type="button" class="btn btn-success btn-xs" data-toggle="collapse" data-target="#demo" style="margin-left:3%;margin-bottom:3%">Ver Detalle</button>
  <div id="demo" class="collapse">
					<div class="panel panel-body">
				<div class="col-md-3">
					<table class="table table-condensed">
						<thead>
							<th style="width:5%">Habilidades</th>
							<th style="width:10%">%</th>
						</thead>
						@foreach($habilidades as $key => $re)
						<tbody>

							<td>{{$key}}</td>
							<td>{{$re}}</td>

						</tbody>
							@endforeach
					</table>
				</div>

				<div class="col-md-3">
					<table class="table table-condensed">
						<thead>
							<th style="width:5%">Competencias</th>
							<th style="width:10%">%</th>
						</thead>
						@foreach($competencias as $key => $value)
						<tbody>
							<td>{{$key}}</td>
							<td>{{$value}}</td>
						</tbody>
							@endforeach
					</table>
				</div>

				<div class="col-md-3">
					<table class="table table-condensed">
						<thead>
							<th style="width:5%">Necesidades</th>
							<th style="width:10%">%</th>
						</thead>
						@foreach($necesidades as $key => $ne)
						<tbody>

							<td>{{$key}}</td>
							<td>{{$ne}}</td>

						</tbody>
							@endforeach
					</table>
				</div>

				<div class="col-md-3">
					<table class="table table-condensed">
						<thead>
							<th style="width:5%">Ranking 4 Area</th>
						</thead>
						@foreach($myArea as $key => $value)
						<tbody>

							<td>{{$key}}</td>
							<td>{{$value}}</td>

						</tbody>
							@endforeach
					</table>
				</div>

				<div class="col-md-3">
					<table class="table table-condensed">
						<thead>
							<th style="width:5%">Best Area</th>
						</thead>
						@foreach($bestArea as $key => $value)
						<tbody>

							<td>{{$value}}</td>

						</tbody>
							@endforeach
					</table>
				</div>
			</div>
  </div>




	</div>




@endsection

@push('script-footer')


<script type="text/javascript">

	function botonContinuar(numero){
		var aux = numero+1;
		$('#panel_'+numero).css("display","none");
		$("#panel_"+aux).css("display","");
		$("html, body").animate({
	        scrollTop: 0
	    }, 1000);
	}

	function botonVolver(numero){
		var aux = numero-1;
		$('#panel_'+numero).css("display","none");
		$("#panel_"+aux).css("display","");
		$("html, body").animate({
	        scrollTop: 0
	    }, 1000);
	}

	$(document).ready(function(){

		$("#formularios-li").addClass( "active" );

	});
</script>
@endpush


<!--
  <div class="col-md-3">
        <table class="table table-striped">
          <thead>
            <th style="width:5%">Best Area</th>
          </thead>
          @foreach($bestArea as $key => $value)
          <tbody>

            <td>{{$value}}</td>

          </tbody>
            @endforeach
        </table>
      </div>

       <div class="col-md-3">
        <table class="table table-striped">
          <thead>
            <th style="width:5%">Needs</th>
          </thead>
          @foreach($jerarquias as $key => $value)
          <tbody>
            <td>{{$key}}</td>
            <td>{{$value}}</td>

          </tbody>
            @endforeach
        </table>
      </div>

  <div class="col-md-3">
        <table class="table table-striped">
          <thead>
            <th style="width:5%">Habilidades</th>
            <th style="width:10%">%</th>
          </thead>
          @foreach($habilidades as $key => $re)
          <tbody>

            <td>{{$key}}</td>
            <td>{{$re}}</td>

          </tbody>
            @endforeach
        </table>
      </div>

      <div class="col-md-3">
        <table class="table table-striped">
          <thead>
            <th style="width:5%">Competencias</th>
            <th style="width:10%">%</th>
          </thead>
          @foreach($competencias as $key => $value)
          <tbody>
            <td>{{$key}}</td>
            <td>{{$value}}</td>
          </tbody>
            @endforeach
        </table>
      </div>

      <div class="col-md-3">
        <table class="table table-striped">
          <thead>
            <th style="width:5%">Necesidades</th>
            <th style="width:10%">%</th>
          </thead>
          @foreach($necesidades as $key => $ne)
          <tbody>

            <td>{{$key}}</td>
            <td>{{$ne}}</td>

          </tbody>
            @endforeach
        </table>
      </div>

      <div class="col-md-3">
        <table class="table table-striped">
          <thead>
            <th style="width:5%">Ranking 4 Area</th>
          </thead>
          @foreach($myArea as $key => $value)
          <tbody>

            <td>{{$key}}</td>
            <td>{{$value}}</td>

          </tbody>
            @endforeach
        </table>
      </div>

  -->
