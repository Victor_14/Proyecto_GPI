@extends('master')

@section('title','Formulario')

@section('content')
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title"><i class="fa fa-file-text-o" aria-hidden="si"></i> Formulario</h3>
	  </div>
	  <div class="panel-body" id="panel_1">
	  	<center>
	  		<h4>Estas en la seccion de "Perfil segun cuestionario"</h4>
	  		<h5>Presiona comenzar para iniciar el cuestionario,pulsa cancelar para volver al Home</h5>
	  		<a href="{{ asset('/') }}" class="btn btn-danger btn-lg" role="button"><i class="fa fa-times" aria-hidden="si"></i> Cancelar</a>
	  		<button type="button" class="btn btn-primary btn-lg" onclick="botonContinuar(1)"><i class="fa fa-check" aria-hidden="si"></i> Comenzar</button>
	  	</center>


	  </div>
	  <form method="post" id="form_formularios" action="{{ url("formularios") }}">
		  <div class="panel-body" id="panel_2" style="display: none">
		  	<center>
		  		<h4>Preguntas</h4>
			  	<p>Responda sinceramente</p>
		  		<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>1. ¿Crees que llegar a acuerdos comunes es la única forma de resolver conflictos? A diferencia de la imposición o la competencia.
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_1" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_1" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>2. ¿Sueles omitir tu opinión en discusiones con el objetivo de lograr llegar a acuerdos de manera más rápida?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_2" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_2" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>3. ¿Necesitas de un ambiente limpio y ordenado para poder llevar a cabo tus actividades?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_3" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_3" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>4. ¿Crees que es necesario poseer métodos bien estructurados para llevar a cabo cualquier actividad?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_4" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;">
				      <input type="radio" name="pregunta_4" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>5. ¿Planeas frecuentemente los objetivos que quieres cumplir para tu futuro?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_5" value="a" checked>Siempre
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_5" value="b">Casi Siempre
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_5" value="c">Regularmente
				    </label>
						<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_5" value="d">De vez en cuando
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_5" value="e">Nunca
				    </label>
				</div>
		  		<button type="button" class="btn btn-warning" onclick="botonVolver(2)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
		  		<button type="button" class="btn btn-primary" onclick="botonContinuar(2)"><i class="fa fa-check" aria-hidden="si"></i> Continuar</button>
		  	</center>
		  </div>
		  <div class="panel-body" id="panel_3" style="display: none">
		  	<center>
		  		<h4>Preguntas</h4>
			  	<p>Responda sinceramente</p>
		  		<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>6. ¿Te interesa plantear objetivos para tu futuro que sean mucho más realistas que ideales?</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_6" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_6" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>7. Al plantearte objetivos personales ¿construyes una serie de pasos a seguir para concretarlos?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_7" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_7" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>8. ¿Prefieres plantearte metas a largo plazo que el vivir el día a día?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_8" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_8" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>9. ¿Te esfuerzas por estar rodeado de gente que sienta la necesidad de autodesarrollarse? </strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_9" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_9" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>10. ¿Te molesta la gente que no se esfuerza por mejorar sus condiciones de vida? </strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_10" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_10" value="no">No
				    </label>
				</div>
		  		<button type="button" class="btn btn-warning" onclick="botonVolver(3)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
		  		<button type="button" class="btn btn-primary" onclick="botonContinuar(3)"><i class="fa fa-check" aria-hidden="si"></i> Continuar</button>
		  	</center>
		  </div>
		  <div class="panel-body" id="panel_4" style="display: none">
		  	<center>
		  		<h4>Preguntas</h4>
			  	<p>Responda sinceramente</p>
		  		<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>11. ¿Disfrutas soñando cómo podría ser tu vida en unos años más?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_11" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_11" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>12. ¿Te excita pensar en las nuevas tecnologías que se desarrollaran en el futuro?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_12" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_12" value="no">No
				    </label>

				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>13. ¿Ideas cosas nuevas, ya sean rutas, metodologías o herramientas, que te ayuden a mejorar tu vida cotidiana? </strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_13" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_13" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>14. ¿Te interesa conocer a fondo el funcionamiento de las cosas para poder aprovecharlas al máximo?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_14" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_14" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>15. ¿Te molesta cuando se decide llevar a cabo una actividad pero nadie pone una fecha de inicio?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_15" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_15" value="no">No
				    </label>
				</div>
		  		<button type="button" class="btn btn-warning" onclick="botonVolver(4)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
		  		<button type="button" class="btn btn-primary" onclick="botonContinuar(4)"><i class="fa fa-check" aria-hidden="si"></i> Continuar</button>
		  	</center>
		  </div>
		  <div class="panel-body" id="panel_5" style="display: none">
		  	<center>
		  		<h4>Preguntas</h4>
			  	<p>Responda sinceramente</p>
		  		<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>16. ¿Prefieres llevar a cabo los planes que pensarlos?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_16" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_16" value="no">No
				    </label>
				</div>
				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>17. ¿Te gusta seguir una rutina estricta para poder lograr hacer las cosas con el tiempo idóneo?</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_17" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_17" value="no">No
				    </label>
				</div>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>18. ¿Crees que organizar el tiempo de cada actividad de un proyecto es más importante que el desarrollo en sí?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_18" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_18" value="no">No
				    </label>
				</div>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>19. ¿Te sientes estresado al saber que vas a llegar atrasado a juntarte con algún amigo?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_19" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_19" value="no">No
				    </label>
				</div>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>20. ¿Sientes que es necesario compensar a las personas cuando no has cumplidos un compromiso con ellas?
</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_20" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_20" value="no">No
				    </label>
				</div>
				<button type="button" class="btn btn-warning" onclick="botonVolver(5)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
				<button type="button" class="btn btn-primary" onclick="botonContinuar(5)"><i class="fa fa-check" aria-hidden="si"></i> Continuar</button>
		  	</center>
		  </div>

			<div class="panel-body" id="panel_6" style="display: none">
			 <center>

				 <div class="container" style="padding-bottom: 20px;">
				 		<h5><strong>21. ¿Prefieres arreglar cualquier máquina o herramienta dañada antes que comprar una nueva?</strong></h5>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_21" value="si" checked>Si
				 		</label>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_21" value="no">No
				 		</label>
				 </div>

				 <div class="container" style="padding-bottom: 20px;">
				 		<h5><strong>22. ¿Crees que es más importante el conocimiento que se puede obtener al reparar un sistema dañado que el tiempo que se ahorra en cambiarlo?
				 </strong></h5>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_22" value="si" checked>Si
				 		</label>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_22" value="no">No
				 		</label>
				 </div>


				 <div class="container" style="padding-bottom: 20px;">
				 		<h5><strong>23.¿Se te hace más fácil o disfrutas más desarrollando actividades cuando estas se presentan como una competencia? </strong></h5>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_23" value="si" checked>Si
				 		</label>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_23" value="no">No
				 		</label>
				 </div>

				 <div class="container" style="padding-bottom: 20px;">
				 		<h5><strong>24.Trabajando en grupo, ¿Cuál de estas acciones prefieres?
				 			</strong></h5>
				 		<label class="radio" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_24" value="a" checked>Generar un ambiente de trabajo colaborativo
				 		</label>
				 		<label class="radio" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_24" value="b">Ordenar al equipo para cumplir los objetivos
				 		</label>
						 <label class="radio" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_24" value="c">Mi interés va orientado a ser el mejor del grupo
				 		</label>
				 </div>

				 <div class="container" style="padding-bottom: 20px;">
				 		<h5><strong>25. ¿Crees que es más importante poseer tiempo libre que ganar más dinero?
				 </strong></h5>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_25" value="si" checked>Si
				 		</label>
				 		<label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 			<input type="radio" name="pregunta_25" value="no">No
				 		</label>
				 </div>

				 <button type="button" class="btn btn-warning" onclick="botonVolver(6)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
				 <button type="button" class="btn btn-primary" onclick="botonContinuar(6)"><i class="fa fa-check" aria-hidden="si"></i> Continuar</button>
			 </center>
		 </div>

		 <div class="panel-body" id="panel_7" style="display: none">
			<center>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>26. ¿Prefieres llevar a cabo actividades placenteras, aunque sean riesgosas, que velar por tu salud? </strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_26" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_26" value="no">No
				    </label>
				</div>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>27. ¿Ideas y llevas a cabo proyectos propios frecuentemente?</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_27" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_27" value="no">No
				    </label>
				</div>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>28. ¿Te sientes a gusto recibiendo órdenes? </strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_28" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_28" value="no">No
				    </label>
				</div>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>29. ¿Prefieres escuchar a los miembros de un equipo o que tan solo sigan tus ordenes?
 						</strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_29" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_29" value="no">No
				    </label>
				</div>

				<div class="container" style="padding-bottom: 20px;">
			  		<h5><strong>30. ¿Crees qué es más importante un buen sueldo que la posibilidad de escalar en la empresa? </strong></h5>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_30" value="si" checked>Si
				    </label>
				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
				      <input type="radio" name="pregunta_30" value="no">No
				    </label>
				</div>

				<button type="button" class="btn btn-warning" onclick="botonVolver(7)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
				<button type="button" class="btn btn-primary" onclick="botonContinuar(7)"><i class="fa fa-check" aria-hidden="si"></i> Continuar</button>
			</center>
		</div>

		<div class="panel-body" id="panel_8" style="display: none">
		 <center>

			 					<div class="container" style="padding-bottom: 20px;">
			 			  		<h5><strong>31. ¿Consideras grato el supervisar el trabajo de otras personas?
			 						</strong></h5>
			 				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
			 				      <input type="radio" name="pregunta_31" value="si" checked>Si
			 				    </label>
			 				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
			 				      <input type="radio" name="pregunta_31" value="no">No
			 				    </label>
			 					</div>

			 					<div class="container" style="padding-bottom: 20px;">
			 			  		<h5><strong>32.¿Crees que las personas deben ser supervisadas para hacer su trabajo?
			  					</strong></h5>
			 				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
			 				      <input type="radio" name="pregunta_32" value="si" checked>Si
			 				    </label>
			 				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
			 				      <input type="radio" name="pregunta_32" value="no">No
			 				    </label>
			 					</div>

			 					<div class="container" style="padding-bottom: 20px;">
			 			  		<h5><strong>33.¿Consideras grato administrar las tareas y responsabilidades de equipos a tu cargo?
			  					</strong></h5>
			 				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
			 				      <input type="radio" name="pregunta_33" value="si" checked>Si
			 				    </label>
			 				    <label class="radio-inline" style="padding-right: 5px;padding-bottom: 5px;">
			 				      <input type="radio" name="pregunta_33" value="no">No
			 				    </label>
			 					</div>



							<button type="button" class="btn btn-warning" onclick="botonVolver(8)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
							<button type="button" class="btn btn-primary" onclick="botonContinuar(8)"><i class="fa fa-check" aria-hidden="si"></i> Continuar</button>

		 </center>
	 </div>

	 <div class="panel-body" id="panel_9" style="display: none">
	 		<center>

				<div class="container" style="padding-bottom: 20px;">
			 			  		<h5><strong> De las siguientes herramientas, indique cuales sabe como utilizar
			 						</strong></h5>
			 				    <div class="col-md-12">
										<label class="checkbox-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 				      <input type="checkbox" name="competencia_1" value="si" >Photoshop
				 				    </label>
				 				    <label class="checkbox-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 				      <input type="checkbox" name="competencia_2" value="si">Maya/Blender
				 				    </label>
										<label class="checkbox-inline" style="padding-right: 5px;padding-bottom: 5px;">
				 				      <input type="checkbox" name="competencia_3" value="si">Matématicas
				 				    </label>
			 				    </div>
									<div class="col-md-12">
										<label class="checkbox-inline" style="padding-right: 5px;padding-bottom: 5px;">
 			 				      <input type="checkbox" name="competencia_4" value="si">Herramientas de Ofímatica
 			 				    </label>
 									 <label class="checkbox-inline" style="padding-right: 5px;padding-bottom: 5px;">
 			 				      <input type="checkbox" name="competencia_5" value="si">Unity
 			 				    </label>
									</div>
									<div class="col-md-12">
									<label class="checkbox-inline" style="padding-right: 5px;padding-bottom: 5px;">
 			 				      <input type="checkbox" name="competencia_7" value="si">Lenguajes de Programación(C#, C++ o JavaScript)
 			 				    </label>
									</div>


			 				</div>
							 <button type="button" class="btn btn-warning" onclick="botonVolver(9)"><i class="fa fa-times" aria-hidden="si"></i> Volver</button>
							 <button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="si"></i> Terminar</button>
			 </center>

		</div>





		  {{ csrf_field() }}
	  </form>
	</div>




@endsection

@push('script-footer')


<script type="text/javascript">

	function botonContinuar(numero){
		var aux = numero+1;
		$('#panel_'+numero).css("display","none");
		$("#panel_"+aux).css("display","");
		$("html, body").animate({
	        scrollTop: 0
	    }, 1000);
	}

	function botonVolver(numero){
		var aux = numero-1;
		$('#panel_'+numero).css("display","none");
		$("#panel_"+aux).css("display","");
		$("html, body").animate({
	        scrollTop: 0
	    }, 1000);
	}

	$(document).ready(function(){

		$("#formularios-li").addClass( "active" );

	});
</script>
@endpush
